export default {
    initWebSocket(path, fn) {
        let open = () => {
            console.log("socket连接成功");
        };
        let error = () => {
            console.log("连接错误");
        };
        let getMessage = fn;
        let close = () => {
            console.log("socket已经关闭");
        };
        if (typeof WebSocket === "undefined") {
            alert("您的浏览器不支持socket");
        } else {
            let socket = "";
            // 实例化socket
            socket = new WebSocket(path);
            // 监听socket连接
            socket.onopen = open;
            // 监听socket错误信息
            socket.onerror = error;
            // 监听socket消息
            socket.onmessage = getMessage;
            socket.onclose = close;
            return socket;
        }
    },
    // 13位时间戳转换年月日时分秒-时间戳-
    //返回值=2021-04-16 10:03:26 
    //全局使用this.$gf.formatDate()
    formatDate(timeStamp = new Date().getTime()) {
        timeStamp = Number(timeStamp);
        let year = new Date(timeStamp).getFullYear();
        let month =
            new Date(timeStamp).getMonth() + 1 < 10 ?
                "0" + (new Date(timeStamp).getMonth() + 1) :
                new Date(timeStamp).getMonth() + 1;
        let date =
            new Date(timeStamp).getDate() < 10 ?
                "0" + new Date(timeStamp).getDate() :
                new Date(timeStamp).getDate();
        let hh =
            new Date(timeStamp).getHours() < 10 ?
                "0" + new Date(timeStamp).getHours() :
                new Date(timeStamp).getHours();
        let mm =
            new Date(timeStamp).getMinutes() < 10 ?
                "0" + new Date(timeStamp).getMinutes() :
                new Date(timeStamp).getMinutes();
        let ss =
            new Date(timeStamp).getSeconds() < 10 ?
                "0" + new Date(timeStamp).getSeconds() :
                new Date(timeStamp).getSeconds();
        let nowTime =
            year + "-" + month + "-" + date + " " + hh + ":" + mm + ":" + ss;
        return nowTime;
    },
    // 当前时间转换第几周
    getWeekOfYear() {
        let today = new Date();
        let firstDay = new Date(today.getFullYear(), 0, 1);
        let dayOfWeek = firstDay.getDay();
        let spendDay = 1;
        if (dayOfWeek != 0) {
            spendDay = 7 - dayOfWeek + 1;
        }
        firstDay = new Date(today.getFullYear(), 0, 1 + spendDay);
        let d = Math.ceil((today.valueOf() - firstDay.valueOf()) / 86400000);
        let result = Math.ceil(d / 7);
        return result + 1;
    },
    // 当前时间转换周几
    getCurrentDate() {
        let today = new Date();
        let days = today.getDay();
        switch (days) {
            case 1:
                days = '星期一';
                break;
            case 2:
                days = '星期二';
                break;
            case 3:
                days = '星期三';
                break;
            case 4:
                days = '星期四';
                break;
            case 5:
                days = '星期五';
                break;
            case 6:
                days = '星期六';
                break;
            case 0:
                days = '星期日';
                break;
        }
        let str = days;
        return str;
    },
    // 身份证号转出生年月  1965-1-22
    getBirthdayFromIdCard(idCard) {
        var birthday = "";
        if (idCard != null && idCard != "") {
            if (idCard.length == 15) {
                birthday = "19" + idCard.substr(6, 6);
            } else if (idCard.length == 18) {
                birthday = idCard.substr(6, 8);
            }

            birthday = birthday.replace(/(.{4})(.{2})/, "$1-$2-");
        }

        return birthday;
    },
    /**
    * @param {number} time
    * @returns {string}
    * 刚刚--12分钟前--3小时前--1天前--2018-11-12 17:58:25
    */
    formatTime(time) {
        if (('' + time).length === 10) {
            time = parseInt(time) * 1000
        } else {
            time = +time
        }
        const d = new Date(time)
        const now = Date.now()

        const diff = (now - d) / 1000

        if (diff < 30) {
            return '刚刚'
        } else if (diff < 3600) {
            // less 1 hour
            return Math.ceil(diff / 60) + '分钟前'
        } else if (diff < 3600 * 24) {
            return Math.ceil(diff / 3600) + '小时前'
        } else if (diff < 3600 * 24 * 2) {
            return '1天前'
        } else {
            return (
                d.getFullYear() + '-' +
                d.getMonth() +
                1 +
                '-' +
                d.getDate() +
                ' ' +
                d.getHours() +
                ':' +
                d.getMinutes() +
                ':' + d.getSeconds()
            )
        }
    },

    /**
    * @param {string} url
    * @returns {Object}
    * 使用：this.$gf.getQueryObject("https://fanyi.baidu.com/?aldtype=16047")
    * 输出：{aldtype: "16047"}
    */
    getQueryObject(url) {
        url = url == null ? window.location.href : url
        const search = url.substring(url.lastIndexOf('?') + 1)
        const obj = {}
        const reg = /([^?&=]+)=([^?&=]*)/g
        search.replace(reg, (rs, $1, $2) => {
            const name = decodeURIComponent($1)
            let val = decodeURIComponent($2)
            val = String(val)
            obj[name] = val
            return rs
        })
        return obj
    },
    // 日期年月日
    formatDateYmd(timeStamp = new Date().getTime()) {
        timeStamp = Number(timeStamp);
        let year = new Date(timeStamp).getFullYear();
        let month =
            new Date(timeStamp).getMonth() + 1 < 10 ?
                "0" + (new Date(timeStamp).getMonth() + 1) :
                new Date(timeStamp).getMonth() + 1;
        let date =
            new Date(timeStamp).getDate() < 10 ?
                "0" + new Date(timeStamp).getDate() :
                new Date(timeStamp).getDate();
        let hh =
            new Date(timeStamp).getHours() < 10 ?
                "0" + new Date(timeStamp).getHours() :
                new Date(timeStamp).getHours();
        let mm =
            new Date(timeStamp).getMinutes() < 10 ?
                "0" + new Date(timeStamp).getMinutes() :
                new Date(timeStamp).getMinutes();
        let ss =
            new Date(timeStamp).getSeconds() < 10 ?
                "0" + new Date(timeStamp).getSeconds() :
                new Date(timeStamp).getSeconds();
        let nowTime =
            year + '年' + month + "月" + date + "日";
        return nowTime;
    },
    // 时分秒
    formatDateHms(timeStamp = new Date().getTime()) {
        timeStamp = Number(timeStamp);
        let year = new Date(timeStamp).getFullYear();
        let month =
            new Date(timeStamp).getMonth() + 1 < 10 ?
                "0" + (new Date(timeStamp).getMonth() + 1) :
                new Date(timeStamp).getMonth() + 1;
        let date =
            new Date(timeStamp).getDate() < 10 ?
                "0" + new Date(timeStamp).getDate() :
                new Date(timeStamp).getDate();
        let hh =
            new Date(timeStamp).getHours() < 10 ?
                "0" + new Date(timeStamp).getHours() :
                new Date(timeStamp).getHours();
        let mm =
            new Date(timeStamp).getMinutes() < 10 ?
                "0" + new Date(timeStamp).getMinutes() :
                new Date(timeStamp).getMinutes();
        let ss =
            new Date(timeStamp).getSeconds() < 10 ?
                "0" + new Date(timeStamp).getSeconds() :
                new Date(timeStamp).getSeconds();
        let nowTime =
            hh + ":" + mm + ":" + ss;
        return nowTime;
    },
    //转换文件大小计算
    getFileSize(size) {
        if (!size) return 0;
        var num = 1024.00;//byte
        if (size < num) {
            return size + 'B';
        } else if (size < Math.pow(num, 2)) {
            return (size / num).toFixed(2) + 'K';//kb
        } else if (size < Math.pow(num, 3)) {
            return (size / Math.pow(num, 2)).toFixed(2) + 'M';//mb
        } else if (size < Math.pow(num, 4)) {
            return (size / Math.pow(num, 3)).toFixed(2) + 'G';//gb
        }else{
            return (size / Math.pow(num, 4)).toFixed(2) + 'T';//t
        }
    }
}
