import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
// 引入全局样式
import './style/index.scss'
// 引入全局echarts
import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts

// 时间格式转换
import moment from 'moment'
Vue.prototype.$moment = moment;//赋值使用
// 引入element-ui
import ElementUI from 'element-ui' //element-ui的全部组件
import 'element-ui/lib/theme-chalk/index.css'//element-ui的css
Vue.use(ElementUI) //使用elementUI

//引入全局方法
import globalFunction from "@/lib/glFun.js";
Vue.prototype.$gf = globalFunction;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
