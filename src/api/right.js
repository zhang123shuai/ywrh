import axios from 'axios';
import qs from 'qs'
// 服务类型
export function serviceType() {
    return axios({
      method: 'get',
      url: `${apiUrlConfig}/rest/bigScreen/serviceType`,
    })
  }
  export function highScoreApp(params) {
    return axios({
      method: 'get',
      url: `${apiUrlConfig}/rest/bigScreen/highScoreApp/${params.limit}`,
    })
  }
  // 用户评价
export function commentSum(params) {
    return axios({
      method: 'get',
      url: `${apiUrlConfig}/rest/bigScreen/commentSum/${params.num}`,
    })
  }
  // 用户评价
export function getCommentList(data) {
    return axios({
      method: 'post',
      url: `${apiUrlConfig}/rest/manage/appInfo/listComment`,
      data: qs.stringify(data)
    })
  }