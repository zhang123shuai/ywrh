import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/situation'
import admin from '../views/backstage'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/admin',
    name: 'admin',
    component: admin
  }
]

const router = new VueRouter({
  routes
})

export default router