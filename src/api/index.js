import axios from 'axios';

// App用户量  limit为显示的行数 time为天数取值1,7,30
export function appUser(params) {
    return axios({
      method: 'get',
      url: `${apiUrlConfig}/rest/bigScreen/appUser/${params.time}/${params.limit}`,
    })
  }

  // 应用启动变化趋势   limit为横坐标天数的数量
export function appStartStatistics(params) {
  return axios({
    method: 'get',
    url: `${apiUrlConfig}/rest/bigScreen/appStartStatistics/${params.limit}`,
  })
}

// 已上线服务数
export function onlineService() {
  return axios({
    method: 'get',
    url: `${apiUrlConfig}/rest/bigScreen/onlineService`,
  })
}
// 正在运行服务数
export function activeService() {
  return axios({
    method: 'get',
    url: `${apiUrlConfig}/rest/bigScreen/activeService`,
  })
}
// 已故障待维修服务数
export function errorService(params) {
  return axios({
    method: 'get',
    url: `${apiUrlConfig}/rest/bigScreen/errorService`,
  })
}

// 交互实时播报
export function getEvent(params) {
  return axios({
    method: 'get',
    url: `${apiUrlConfig}/rest/bigScreen/getEvent/${params.limit}`,
  })
}

// 操作数
export function getCount(params) {
  return axios({
    method: 'get',
    url: `${apiUrlConfigyw}/yunwang/getCount/${params.limit}`,
  })
}
// 模拟单条数据开始
export function yunwangPushOne() {
  return axios({
    method: 'get',
    url: `${apiUrlConfigyw}/yunwang/pushOne`,
  })
}
// 模拟多条数据开始调用
export function yunwangPushMulty() {
  return axios({
    method: 'get',
    url: `${apiUrlConfigyw}/yunwang/pushMulty`,
  })
}
// 模拟多条数据关闭
export function yunwangonOffoff() {
  return axios({
    method: 'get',
    url: `${apiUrlConfigyw}/yunwang/onOff/off`,
  })
}
// 模拟多条数据开始
export function yunwangonOffon() {
  return axios({
    method: 'get',
    url: `${apiUrlConfigyw}/yunwang/onOff/on`,
  })
}
//获取全部车辆信息
export function yunwanggetVehicle(params) {
  return axios({
    method: 'get',
    url: `${apiUrlConfigyw}/yunwang/getVehicle/${params.limit}`,
  })
}
//获取全部旅团信息
export function yunwanggetUnit(params) {
  return axios({
    method: 'get',
    url: `${apiUrlConfigyw}/yunwang/getUnit/${params.limit}`,
  })
}